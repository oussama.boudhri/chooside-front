import { ChoosideFrontPage } from './app.po';

describe('chooside-front App', function() {
  let page: ChoosideFrontPage;

  beforeEach(() => {
    page = new ChoosideFrontPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
