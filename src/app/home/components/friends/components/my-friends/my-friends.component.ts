import { Component, OnInit } from '@angular/core';
import { FriendService } from './../../services/friend.service'

@Component({
  selector: 'app-my-friends',
  templateUrl: './my-friends.component.html',
  styleUrls: ['./my-friends.component.css']
})
export class MyFriendsComponent implements OnInit {

  friends = []

  constructor(private friendService: FriendService) { }

  ngOnInit() {
    this.getMyFriends()
  }

  getMyFriends() {
    return this.friendService.getMyFriends().subscribe(friends => this.friends = friends );
  }

}
