import { Component, OnInit } from '@angular/core';
import { FriendService } from '../../services/friend.service'

@Component({
  selector: 'app-proposition-friends',
  templateUrl: './proposition-friends.component.html',
  styleUrls: ['./proposition-friends.component.css']
})
export class PropositionFriendsComponent implements OnInit {

  users = []

 constructor(private friendService: FriendService) { }

  ngOnInit() {
    this.getAvailableUser();
  }

  getAvailableUser() {
    this.friendService.getAvailableUser().subscribe(users => this.users = users );
  }

  addAsFriend(id) {
    this.friendService.addAsFriend(id);
  }

}
