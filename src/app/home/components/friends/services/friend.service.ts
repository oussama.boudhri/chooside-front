import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { AuthenticationService } from '../../../../login/services/authentication.service'

@Injectable()
export class FriendService {
  
  headers = new Headers({
	  'Content-Type': 'application/json',
	  'Authorization': this.authenticationService.token
	});

  constructor(private http: Http, private authenticationService: AuthenticationService) { }

  getAvailableUser() {
    return this.http.get('/api/users', { headers: this.headers })
            .map((res:Response) => res.json()) // ...and calling .json() on the response to return data
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  addAsFriend(id) {
    
    return this.http.get(`/api/addfriend?id=${id}`, { headers: this.headers })
            .subscribe((res:Response) => res.json()) // ...and calling .json() on the response to return data
  }

  getMyFriends() {
     return this.http.get('/api/listfriends', { headers: this.headers })
            .map((res:Response) => res.json()) // ...and calling .json() on the response to return data
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

}
