import { Component, OnInit } from '@angular/core';
import { ProfileService } from './services/profile.service'
import { Observable } from 'rxjs/Observable';
import { Profile } from './model/profile'

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  userInfo:Observable<Profile> = new Observable<Profile>();

  constructor(private profileService: ProfileService) { }

  ngOnInit() {
    this.getInfoProfile()
  }

  getInfoProfile() {
    return this.profileService.getInfo().subscribe(profile => this.userInfo = profile );
  }

}
