import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home.component';
import { AuthGuard } from './../common/auth.guard';
import { ProfileComponent } from './components/profile/profile.component';
import { FriendsComponent } from './components/friends/friends.component';


export const homeRoutes: Routes = [
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard], 
    children: [  
      { path: '', outlet: 'homeOutlet', component: ProfileComponent },
      { path: 'friends', outlet: 'homeOutlet', component: FriendsComponent }
    ] 
  }
];

export const HomeRouting: ModuleWithProviders = RouterModule.forChild(homeRoutes)