import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeComponent } from './home.component';
import { TopnavbarComponent } from './components/topnavbar/topnavbar.component';
import { ProfileComponent } from './components/profile/profile.component';
import { FriendsComponent } from './components/friends/friends.component';
import { PropositionFriendsComponent } from './components/friends/components/proposition-friends/proposition-friends.component';
import { MyFriendsComponent } from './components/friends/components/my-friends/my-friends.component';


import { ProfileService } from './components/profile/services/profile.service';
import { FriendService } from './components/friends/services/friend.service';

import { HomeRouting } from './home.routing';

import { MdButtonModule, MdCardModule, MdMenuModule, MdToolbarModule, MdIconModule, MdInputModule, MdListModule, MdTabsModule } from '@angular/material';


@NgModule({
  imports: [HomeRouting, 
            MdButtonModule, 
            MdCardModule, 
            MdMenuModule, 
            MdToolbarModule, 
            MdIconModule, 
            MdInputModule, 
            MdListModule, 
            MdTabsModule,
            CommonModule],
  declarations: [
    HomeComponent,
    TopnavbarComponent,
    ProfileComponent,
    FriendsComponent,
    PropositionFriendsComponent,
    MyFriendsComponent
  ],
  providers: [ProfileService, FriendService]
})
export class HomeModule { }
