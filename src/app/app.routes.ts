import { Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { SignupComponent } from './signup/signup.component';
import { AuthGuard } from './common/auth.guard';
import { ProfileComponent } from './home/components/profile/profile.component';
import { FriendsComponent } from './home/components/friends/friends.component';
import { ErrorComponent } from './error/error.component';

export const routes: Routes = [
  { path: '',       component: LoginComponent },
  { path: 'login',  component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'home',   component: HomeComponent, canActivate: [AuthGuard], 
    children: [  
      { path: '', outlet: 'homeOutlet', component: ProfileComponent },
      { path: 'friends', outlet: 'homeOutlet', component: FriendsComponent }
    ] 
  },
  { path: '**', component: ErrorComponent }
];
