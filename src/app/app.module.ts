import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule, Http, RequestOptions, XHRBackend } from '@angular/http';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { routes } from './app.routes';

import { MdButtonModule, MdCardModule, MdMenuModule, MdToolbarModule, MdIconModule, MdInputModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import 'hammerjs';

import { provideAuth, AuthHttp, AuthConfig } from 'angular2-jwt';

import { AuthGuard } from './common/auth.guard';
import { AuthenticationService } from './login/services/authentication.service'
import { RegistrationService } from './signup/services/registration.service'

import { ApiXHRBackend } from './common/api-xhr-backend'

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ErrorComponent } from './error/error.component';

import { HomeModule } from './home/home.module';

export function authHttpServiceFactory(http: Http, options: RequestOptions) {
  return new AuthHttp( new AuthConfig({}), http, options);
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    ErrorComponent
  ],
  imports: [
    BrowserModule,
    HomeModule,
    HttpModule,
    RouterModule.forRoot(routes, { useHash: true }),
    MdButtonModule,
    MdCardModule,
    MdMenuModule,
    MdToolbarModule,
    MdIconModule,
    MdInputModule,
    BrowserAnimationsModule,
    ReactiveFormsModule
  ],
  providers: [AuthGuard, AuthenticationService, RegistrationService, 
              {provide: AuthHttp, useFactory: authHttpServiceFactory, deps: [ Http, RequestOptions ] },
               { provide: XHRBackend, useClass: ApiXHRBackend }],
  bootstrap: [AppComponent]
})
export class AppModule { }
