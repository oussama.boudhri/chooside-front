import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { RegistrationService } from './services/registration.service'
import { User } from './model/user'
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  form: FormGroup;
  model:User 
  error: string ="";

  constructor(private fb: FormBuilder, private registrationService: RegistrationService, private router: Router) {
     this.form = fb.group({
      "email":    ["", Validators.required],
      "password": ["", Validators.required],
      "firstname": ["", Validators.required],
      "lastname": ["", Validators.required],
      "phone": ["", Validators.required]
    })
   }

  ngOnInit() {
  }

  register() {
   
    this.model = {
      email: this.form.get('email').value,
      password: this.form.get('password').value,
      firstname: this.form.get('firstname').value,
      lastname: this.form.get('lastname').value
    }

    this.registrationService.register(this.model)
        .subscribe(result => {
          if (result) {
             this.router.navigate(['/']);
          } else {
            this.error = 'Error in registration';
          }
        },
        error => {
          this.error = 'Error in registration';
        });
  }

}
