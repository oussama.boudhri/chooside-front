import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http'
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class RegistrationService {

  constructor(private http: Http) { }

  register(user) {
    return this.http.post(`/signup?email=${user.email}&password=${user.password}&firstname=${user.firstname}&lastname=${user.lastname}`, JSON.stringify(user))
      .map((response: Response) => {
        let userRegistered = response.json() && response.json().userRegistered;
        if (userRegistered) {
          return true;
        } else {
          return false;
        }

      })
  }
}
