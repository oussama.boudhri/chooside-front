import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from './services/authentication.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  error: string ="";

  constructor(private router:Router, private authenticationService: AuthenticationService, fb: FormBuilder) {
    this.form = fb.group({
      "email":    ["", Validators.required],
      "password": ["", Validators.required]
    })
  }

  ngOnInit() {
    this.authenticationService.logout();
  }

  login () {
    this.authenticationService.login(this.form.get('email').value, this.form.get('password').value)
        .subscribe(result => {
          if (result) {
            console.log(result)
            this.router.navigate(['/home']);
          } else {
            this.error = 'Username or password is incorrect';
          }
        },
        error => {
          this.error = 'Username or password is incorrect';
        });
  }

}
