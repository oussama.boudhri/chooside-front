import {Injectable} from '@angular/core';
import {Request, XHRBackend, XHRConnection} from '@angular/http';

@Injectable()
export class ApiXHRBackend extends XHRBackend {

    baseUrl: String = 'http://localhost:9090'

    createConnection(request: Request): XHRConnection {
        if (request.url.startsWith('/')){
            request.url = this.baseUrl + request.url;     // prefix base url
        }
        return super.createConnection(request);
    }
}